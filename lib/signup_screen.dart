import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

import 'successful_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
 // runApp(SignupScreen());
}

class SignupScreen extends StatefulWidget {
  static const routeName = 'signup-screen';

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController reenterPasswordController =
  TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool passwordVisible = false;
  bool reenterPasswordVisible = false;

  Future<void> _signupUser(BuildContext context) async {
    try {
      final String email = emailController.text;
      final String password = passwordController.text;
      final String reenterPassword = reenterPasswordController.text;

      if (email.isEmpty || password.isEmpty || reenterPassword.isEmpty) {
        // Display an error message if any field is empty
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Please fill in all fields.'),
          ),
        );
        return;
      }

      if (password != reenterPassword) {
        // Display an error message if passwords don't match
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Passwords do not match.'),
          ),
        );
        return;
      }

      // Create a new user with email and password
      final UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      if (userCredential.user != null) {
        // Redirect to the successful signup screen upon successful registration
        Navigator.of(context).push(
          MaterialPageRoute(builder: (ctx) => SuccessfulScreen()),
        );
      }
    } catch (error) {
      // Handle signup errors
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('An error occurred. Please try again.'),
        ),
      );
    }
  }

  Widget signUpWith(IconData icon) {
    return Container(
      height: 50,
      width: 115,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey.withOpacity(0.4), width: 1),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, size: 24),
          TextButton(onPressed: () {}, child: Text('Sign in')),
        ],
      ),
    );
  }

  Widget userInput(
      TextEditingController userInput,
      String hintTitle,
      TextInputType keyboardType,
      bool obscureText,
      ) {
    return Container(
      height: 55,
      margin: EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
        color: Colors.blueGrey.shade200,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 25.0, top: 15, right: 25),
        child: TextField(
          controller: userInput,
          autocorrect: false,
          enableSuggestions: false,
          autofocus: false,
          obscureText: obscureText,
          decoration: InputDecoration.collapsed(
            hintText: hintTitle,
            hintStyle: TextStyle(
              fontSize: 18,
              color: Colors.white70,
              fontStyle: FontStyle.italic,
            ),
          ),
          keyboardType: keyboardType,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(height: 45),
                      userInput(
                        emailController,
                        'Email',
                        TextInputType.emailAddress,
                        false,
                      ),
                      userInput(
                        passwordController,
                        'Password',
                        TextInputType.visiblePassword,
                        !passwordVisible,
                      ),
                      Container(
                        height: 55,
                        margin: EdgeInsets.only(bottom: 15),
                        decoration: BoxDecoration(
                          color: Colors.blueGrey.shade200,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Padding(
                          padding:
                          const EdgeInsets.only(left: 25.0, top: 15, right: 25),
                          child: Stack(
                            alignment: Alignment.centerRight,
                            children: [
                              TextField(
                                controller: reenterPasswordController,
                                autocorrect: false,
                                enableSuggestions: false,
                                autofocus: false,
                                obscureText: !reenterPasswordVisible,
                                decoration: InputDecoration.collapsed(
                                  hintText: 'Re-enter Password',
                                  hintStyle: TextStyle(
                                    fontSize: 17,
                                    color: Colors.white70,
                                    fontStyle: FontStyle.italic,
                                  ),
                                ),
                                keyboardType: TextInputType.visiblePassword,
                              ),

                              IconButton(
                                icon: Icon(
                                  reenterPasswordVisible
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                  color: Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() {
                                    reenterPasswordVisible =
                                    !reenterPasswordVisible;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 55,
                        padding:
                        const EdgeInsets.only(top: 5, left: 70, right: 70),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25),
                            ),
                            backgroundColor: Colors.indigo.shade800,
                          ),
                          onPressed: () => _signupUser(context),
                          child: Text(
                            'Sign up',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

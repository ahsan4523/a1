import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final CollectionReference _attendanceCollection =
  FirebaseFirestore.instance.collection('attendance');

  bool attendanceMarked = false;
  List<String> attendanceList = [];
  File? _profilePicture;

  void markAttendance() async {
    if (!attendanceMarked) {
      try {
        User? user = _auth.currentUser;
        if (user != null) {
          String userId = user.uid;
          DateTime currentTime = DateTime.now();

          await _attendanceCollection.doc(userId).set({
            'userId': userId,
            'attendanceTime': currentTime,
          });

          setState(() {
            attendanceList.add(currentTime.toString());
            attendanceMarked = true;
          });
        }
      } catch (e) {
        print('Error attendance: $e');
      }
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              padding: EdgeInsets.all(16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Attendance Already Marked',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  SizedBox(height: 16.0),
                  Text('You have already marked your attendance today.'),
                  SizedBox(height: 16.0),
                  ElevatedButton(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
  }

  void viewAttendance() async {
    try {
      User? user = _auth.currentUser;
      if (user != null) {
        String userId = user.uid;

        QuerySnapshot querySnapshot =
        await _attendanceCollection.where('user-Id', isEqualTo: userId).get();

        List<String> history = [];
        for (QueryDocumentSnapshot document in querySnapshot.docs) {
          DateTime attendanceTime = document['attendanceTime'].toDate();
          history.add(attendanceTime.toString());
        }

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Attendance History',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                    SizedBox(height: 16.0),
                    history.isEmpty
                        ? Text('No attendance marked yet.')
                        : ListView.builder(
                      shrinkWrap: true,
                      itemCount: history.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          title: Text(history[index]),
                        );
                      },
                    ),
                    SizedBox(height: 16.0),
                    ElevatedButton(
                      child: Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }
    } catch (e) {
      print('Error viewing attendance: $e');
    }
  }

  Future<void> _changeProfilePicture() async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(
      source: ImageSource.gallery, // Change to ImageSource.camera for camera access
    );

    if (pickedImage != null) {
      setState(() {
        _profilePicture = File(pickedImage.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 80,
                backgroundImage: _profilePicture != null
                    ? FileImage(_profilePicture!)
                    : null,
                child: _profilePicture == null
                    ? SvgPicture.asset(
                  '/home/r/Andriod/front/images/1.jpg',
                  fit: BoxFit.cover,
                )
                    : null,
              ),
              SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: _changeProfilePicture,
                child: Text('Change Profile Picture'),
              ),
              SizedBox(height: 16),
              buildStyledButton('Mark Attendance', Colors.blue, markAttendance),
              SizedBox(height: 16),
              buildStyledButton('View', Colors.blue, viewAttendance),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildStyledButton(String text, Color color, Function() onPressed) {
    return Container(
      width: 200,
      height: 50,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(25),
        border: Border.all(color: color),
      ),
      child: MaterialButton(
        onPressed: onPressed,
        child: Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MaterialApp(home: MyHomePage()));
}
